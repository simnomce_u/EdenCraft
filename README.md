# EdenCraft

[![GitLab Releases](https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://framagit.org/api/v4/projects/24785/repository/tags&query=$[0].name&colorB=blue&logo=gitlab)](https://framagit.org/simnomce_u/EdenCraft/tags)
[![Build Status](https://travis-ci.org/simnomceu/EdenCraft.svg?branch=master)](https://travis-ci.org/simnomceu/EdenCraft)
[![Build status](https://ci.appveyor.com/api/projects/status/h14mj302e5x0amy4/branch/master?svg=true)](https://ci.appveyor.com/project/simnomceu/edencraft/branch/master)
[![Coverage Status](https://coveralls.io/repos/github/simnomceu/EdenCraft/badge.svg?branch=master)](https://coveralls.io/github/Isilin/EdenCraft?branch=master)


## Introduction
The Edencraft Engine is a Free/Libre & Open Source (FLOSS), multiplatform framework for application and game development.

It is build without any external dependencies, excepted the [Catch2 library](https://github.com/catchorg/Catch2) for automated test cases.

The Edencraft Engine provides an API for creating windows for the following windowing systems : X11 (Xlib / XCB), DWM. It also provides an API to use an OpenGL 3D rendering context, with GLX and WGL. OpenGL4.6 is supported.


## Compiling the EdenCraft Engine
This project use [Premake5](https://premake.github.io/download.html) to generate the project files. You need don't need to download Premake5, as it is provided in the build directory. To build the engine, use the premake script, according to your system, then compile it.

## Using the Edencraft Engine
Tutorials and documentation is not available yet. Work in progress.

## Contribution to EdenCraft Engine
Bugs can be reported on the Framagit issue tracker [here](https://framagit.org/simnomce_u/EdenCraft/issues).

Mirrors are available on [Github](https://github.com/simnomceu/EdenCraft) and [Bitbucket](https://bitbucket.org/simnomce_u/edencraft) for read access only (clone and fork). To contribute to the project, please use the [Framagit](https://framagit.org/simnomce_u/EdenCraft) account.

## Authors
* IsilinBN
* Alcyone

## Copyright and Licensing
The EdenCraft Engine is delivered under the [GNU-GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html).

The EdenCraft Engine is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.