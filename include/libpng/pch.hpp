#ifndef LIBPNG_PCH_HPP
#define LIBPNG_PCH_HPP

#ifdef _MSC_VER
#	undef min
#	undef max
#endif

#include "core/format.hpp"
#include "renderer/image.hpp"

#endif // LIBPNG_PCH_HPP