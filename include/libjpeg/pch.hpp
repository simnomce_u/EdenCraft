#ifndef LIBJPEG_PCH_HPP
#define LIBJPEG_PCH_HPP

#ifdef _MSC_VER
#	undef min
#	undef max
#endif

#include "core/format.hpp"
#include "renderer/image.hpp"

#endif // LIBJPEG_PCH_HPP