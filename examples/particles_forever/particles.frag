#version 450
in vec4 particleColor;

out vec4 fragColor;

void main()
{
    fragColor = particleColor;
}
