# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [alpha-0.1.0] - Basics Update - 02/01/2021

### Added

- [x] Scripts automation for project generation (Premake5).

- [x] Doxygen profile.

- [x] Licensing.

- [x] Static & Shared library usages.

- [x] **Utility** - Basic exception specialization.

- [x] **Utility** - Enumeration used like C-like flags.

- [x] **Utility** - Reflective enumeration traits.

- [x] **Utility** - Reading and writing files and binary files.

- [x] **Utility** - Parsing BMP images.

- [x] **Utility** - Parsing OBJ and MTL Wavefront formats.

- [x] **Utility** - Parsing JSON formats.

- [x] **Utility** - Version handling.

- [x] **Utility** - Unique ID generator.

- [x] **Utility** - Localization handling.

- [x] **Utility** - Console/file logger.

- [x] **Utility** - Geometric operations.

- [x] **Utility** - Matrix and vector handling, with expression templates.

- [x] **Utility** - 3D rotation using quaternions, matrix, and Euler angles.

- [x] **Utility** - Pimpl design pattern implementation.

- [x] **Utility** - Listener/listenable design pattern.

- [x] **Utility** - Service-consumer pattern implementation.

- [x] **Utility** - Timer, and UPS/FPS counter.

- [x] **Core** - General application management.

- [x] **Core** - CLI parser.

- [x] **Core** - Entity-Component-System pattern.

- [x] **Core** - Signal/Slot mechanism (Qt-like).

- [x] **Core** - Basic resources manager without any strategy (like, unloading the resource proportionally to the elapsed time since the last use).

- [x] **Window** - Windowed application.

- [x] **Window** - Common interface to handle window.

- [x] **Window** - Enqueuing window events.

- [x] **Window** - Dealing with screen ratio/resolution.

- [x] **Window** - Handling video mode.

- [x] **Window** - Win32 implementation of window.

- [x] **Window** - X11 implementation of window (Xlib and XCB).

- [x] **Window** - Managing input devices events (Mouse, keyboard).

- [x] **Renderer** - Common interface for OpenGL context.

- [x] **Renderer** - OpenGL extensions loader with lazy-loading.

- [x] **Renderer** - OpenGL support from OpenGL 3.3 to OpenGL 4.6.

- [x] **Renderer** - WGL implementation for Win32.

- [x] **Renderer** - GLX implementation for X11.

- [x] **Renderer** - Basic 2D and 3D renderer.

- [x] **Renderer** - Basic shaders.

- [x] **Renderer** - Basic 2D textures.

- [x] **Renderer** - MSAA with OpenGL implementation.

- [x] **Renderer** - Phong ligthing model.

- [x] **Renderer** - Render state used for draw calls.

- [x] **Renderer** - Light emitter (spot, directional, point).

- [x] **Graphic** - Basic particle generator.

- [x] **Graphic** - 3D scene with 3D objects.

- [x] **Graphic** - Static camera.

- [x] **Graphic** - 3D primitive objects (Cube, sphere, rectangle, square, triangle, circle, torus, cylinder, pyramid, tetrahedron, etc ...)

- [x] **Samples** - A set of samples to test some features

### Changed

### Deprecated

### Removed

### Fixed

### Security
