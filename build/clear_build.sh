#!/bin/bash

# Visual Studio project
rm -rf ./vs2015/
rm -rf ./vs2017/
rm -rf ./vs2019/

# CodeLite project
rm -rf ./codelite/

# Compilation files
rm -rf ../bin/
rm -rf ../obj/

# Gmake project
rm -rf ./gmake2/
