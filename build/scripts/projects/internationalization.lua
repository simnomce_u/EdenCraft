#!lua

-- internationalization.lua

local settings = Project:new()

settings:setName("internationalization")
settings:setType("ConsoleApp")
settings:addDependencies{"utility"}

return settings
